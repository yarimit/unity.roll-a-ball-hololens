﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WallController : MonoBehaviour, IManipulationHandler {
	public GameObject PlayArea_;
    public Vector3 RotateVector_;
    private Vector3 ManipulationStartPosition_;


	public void OnManipulationCanceled(ManipulationEventData eventData) {
		//Debug.LogFormat("OnManipulationCanceled\r\nSource: {0}  SourceId: {1}\r\nCumulativeDelta: {2} {3} {4}",
		//	eventData.InputSource,
		//	eventData.SourceId,
		//	eventData.CumulativeDelta.x,
		//	eventData.CumulativeDelta.y,
		//	eventData.CumulativeDelta.z);
	}

	public void OnManipulationCompleted(ManipulationEventData eventData) {
		//Debug.LogFormat("OnManipulationCompleted\r\nSource: {0}  SourceId: {1}\r\nCumulativeDelta: {2} {3} {4}",
		//	eventData.InputSource,
		//	eventData.SourceId,
		//	eventData.CumulativeDelta.x,
		//	eventData.CumulativeDelta.y,
		//	eventData.CumulativeDelta.z);
	}

	public void OnManipulationStarted(ManipulationEventData eventData) {
		Debug.LogFormat("OnManipulationStarted\r\nSource: {0}  SourceId: {1}\r\nCumulativeDelta: {2} {3} {4}",
			eventData.InputSource,
			eventData.SourceId,
			eventData.CumulativeDelta.x,
			eventData.CumulativeDelta.y,
			eventData.CumulativeDelta.z);

        ManipulationStartPosition_ = eventData.CumulativeDelta;
	}

	public void OnManipulationUpdated(ManipulationEventData eventData) {
        //Debug.LogFormat("OnManipulationUpdated\r\nSource: {0}  SourceId: {1}\r\nCumulativeDelta: {2} {3} {4}",
        //	eventData.InputSource,
        //	eventData.SourceId,
        //	eventData.CumulativeDelta.x,
        //	eventData.CumulativeDelta.y,
        //	eventData.CumulativeDelta.z);

        float RotateSensitive_ = 20.0f;
        var vecDelta = Vector3.zero;
        vecDelta = eventData.CumulativeDelta - ManipulationStartPosition_;
        var deltaY = vecDelta.y;
        Debug.LogFormat("deltaY: {0:F6}", deltaY);
        PlayArea_.transform.Rotate(new Vector3(RotateVector_.x * deltaY * RotateSensitive_,
            RotateVector_.y * deltaY * RotateSensitive_,
            RotateVector_.z * deltaY * RotateSensitive_
            ));


        //Vector3 delta = Vector3.zero;
        //delta = eventData.CumulativeDelta - ManipulationStartPosition_;
        ////Debug.LogFormat("delta : {0}", delta);
        //Vector3 _v = new Vector3(transform.position.x, transform.position.y + delta.y, transform.position.z);
        ////Debug.LogFormat("pos: {0} - {1}", transform.position, _v);
        //Debug.LogFormat("pos: {0} - {1}", _v, PlayArea_.transform.position);

        //var curAngle = Vector3.Angle(_v, PlayArea_.transform.position);
        //if(_v.y < PlayArea_.transform.position.y) {
        //    curAngle *= -1;
        //}
        //Debug.LogFormat("angle: {0:F6}", curAngle);
        //PlayArea_.transform.Rotate(new Vector3(curAngle, 0, 0));
        //Debug.LogFormat("angle: {0:F6} - {1:F6}", yangle(transform.position, PlayArea_.transform.position),
        //    yangle(_v, transform.position));
        //PlayArea_.transform.Rotate(new Vector3(1, 0, 0), 1);
        ManipulationStartPosition_ = eventData.CumulativeDelta;
    }

    float yangle(Vector3 from, Vector3 to) {
        Vector3 f = new Vector3(0, from.y, 0);
        Vector3 t = new Vector3(0, to.y, 0);
        return Vector3.Angle(f, t);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
